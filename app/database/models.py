import dataclasses
import utils


@dataclasses.dataclass
class Subject:
    id: int
    title: str
    description: str


@dataclasses.dataclass
class Comment:
    id: int
    title: str
    text: str
    timestamp: float
    subject_id: int

    def format_timestamp(self):
        return utils.format_time(self.timestamp)
