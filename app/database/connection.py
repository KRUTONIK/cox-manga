import sqlite3
import config


def connect():
    return sqlite3.connect(config.DATABASE_FILE)

