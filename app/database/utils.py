import database.connection
import database.models
import utils


def get_comments(subject_id):
    with database.connection.connect() as db:
        return [
            database.models.Comment(*row)
            for row in  db.execute(
                "SELECT * FROM comments WHERE subject_id = ?", [subject_id]
            ).fetchall()
        ]


def add_comment(subject_id, title, text):
    with database.connection.connect() as db:
        db.execute(
            "INSERT INTO comments (title, text, timestamp, subject_id) values (?, ?, ?, ?)",
            [title, text, utils.get_server_time().timestamp(), subject_id],
        )


def add_subject(title, description):
    with database.connection.connect() as db:
        db.execute(
            "INSERT INTO subjects (title, description) values (?, ?)",
            [title, description],
        )


def get_subjects():
    with database.connection.connect() as db:
        return [
            database.models.Subject(*row)
            for row in db.execute("SELECT * FROM subjects").fetchall()
        ]


def get_subject(subject_id):
    with database.connection.connect() as db:
        subject = db.execute(
            "SELECT * FROM subjects WHERE id = ?", [subject_id]
            ).fetchone()

    if subject is None:
        raise ValueError("Subject not found")
    
    return database.models.Subject(*subject)


def delete_comments(subject_id):
    with database.connection.connect() as db:
        db.execute(
            "DELETE FROM comments WHERE subject_id = ?", [subject_id]
            ).fetchall()


def delete_comment(subject_id, comment_id):
    with database.connection.connect() as db:
        db.execute(
            "DELETE FROM comments WHERE subject_id = ? AND id = ?", [subject_id, comment_id]
            ).fetchall()


def delete_subject(id):
    with database.connection.connect() as db:
        db.execute(
            "DELETE FROM subjects WHERE id = ?", [id]
            ).fetchall()


"""
def get_comments():
    with open(config.DATABASE_FILE, encoding="utf-8") as fin:
        return json.load(fin)


def add_comment(title, text):
    comments = get_comments()
    comments.append(
        {
            "title": title,
            "text": text,
        }
    )
    with open(config.DATABASE_FILE, "w", encoding="utf-8") as fout:
        json.dump(comments, fout)

def delete_comments():
    with open(config.DATABASE_FILE, "w", encoding="utf-8") as fout:
        json.dump([], fout)
"""
