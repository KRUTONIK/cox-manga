import flask

import database.utils as db_utils


views = flask.Blueprint("views", __name__)


@views.route("/subjects/<int:subject_id>", methods=["GET", "POST"])
def view_subject(subject_id):
    try:
        subject = db_utils.get_subject(subject_id)
    except ValueError as e:
        flask.abort(404, *e.args)

    if flask.request.method == "POST":
        title = flask.request.form["title"]
        text = flask.request.form["text"]
        db_utils.add_comment(subject_id, title, text)

    comments = db_utils.get_comments(subject_id)
    for comment in comments:
        comment.timestamp

    return flask.render_template(
        "subject.html",
        subject=subject,
        comments=db_utils.get_comments(subject_id),
    )


@views.route("/", methods=["GET", "POST"])
def view_index():
    if flask.request.method == "POST":
        title = flask.request.form["title"]
        description = flask.request.form["description"]
        db_utils.add_subject(title, description)

    return flask.render_template("index.html", subjects=db_utils.get_subjects())


@views.route("/subjects/<int:subject_id>/delete/all")
def delete_all(subject_id):
    db_utils.delete_comments(subject_id)
    return flask.redirect(f"/subjects/{ subject_id }")


@views.route("/subjects/<int:subject_id>/delete/<int:comment_id>")
def delete_one(subject_id, comment_id):
    db_utils.delete_comment(subject_id, comment_id)
    return flask.redirect(f"/subjects/{ subject_id }")


@views.route("/delete/<int:id>")
def delete_subject(id):
    db_utils.delete_subject(id)
    return flask.redirect("/")
